
#!/bin/bash
#https://github.com/nvidia/nvidia-docker/wiki/Installation-(version-2.0)

install_nvidia_docker () {
    set -e
    sudo apt-get update && sudo apt-get install -y curl
    docker volume ls -q -f driver=nvidia-docker | xargs -r -I{} -n1 docker ps -q -a -f volume={} | xargs -r docker rm -f
    #sudo apt-get purge nvidia-docker
    curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
    distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
    curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
    sudo add-apt-repository ppa:graphics-drivers/ppa -y
    sudo apt-get update
    sudo apt-get install -y nvidia-modprobe nvidia-docker2
    sudo pkill -SIGHUP dockerd
}


install_nvidia_docker
