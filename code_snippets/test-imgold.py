#!/usr/bin/env python
import argparse
import os
import pydicom
import nibabel as nib
import matplotlib.pyplot as plt
import sys
import numpy
numpy.set_printoptions(threshold=sys.maxsize)
import binascii
import shutil
import errno

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def is_gz_file(filepath):
    with open(filepath, 'rb') as test_f:
        return binascii.hexlify(test_f.read(2)) == b'1f8b'

def show_slices(slices):
   """ Function to display row of image slices """
   fig, axes = plt.subplots(1, len(slices))
   for i, slice in enumerate(slices):
       axes[i].imshow(slice.T, cmap="gray", origin="lower")

def show_nii(file):
    if is_gz_file(file):
        import gzip
        with gzip.open(file, 'rb') as fz:
            tmp = file + "-unzipped"
            with open(tmp, 'wb') as f:
               f.write(fz.read())
            os.remove(file)
            shutil.move(tmp, file)

    img = nib.load(file)
    # print(img)
    img_data = img.get_fdata()
    sp = img_data.shape
    print("{} shape is {}, dtype={}".format(file, sp, img_data.dtype))
    split_to_png = False
    if split_to_png:
       base = os.path.dirname(file)
       base = os.path.join(base, "nii-split")
       mkdir_p(base)
       from PIL import Image
       for i in range(sp[2]):
          slice_2 = img_data[:, :, i] * 255
          #print(slice_2)
          slice_name = os.path.join(base, "label-{}-{}.png".format(i+1, sp[2]))
          im = Image.fromarray(slice_2.astype(numpy.uint8))
          im.save(slice_name)
    else:
       slice_0 = img_data[sp[0]//2, :, :]
       slice_1 = img_data[:, sp[1]//2, :]
       slice_2 = img_data[:, :, sp[2]//2]

       # print(slice_2)
       show_slices([slice_0, slice_1, slice_2])
       plt.suptitle("Center slices for image")
       plt.show();

def show_dcm(file):
    # from pydicom.data import get_testdata_files
    # filename = get_testdata_files("CT_small.dcm")[0]
    # ds = pydicom.dcmread(filename)
    # plt.imshow(ds.pixel_array, cmap=plt.cm.bone)
    # plt.show();
    ds = pydicom.dcmread(file)
    print(ds)
    print(ds.pixel_array.shape)
    plt.imshow(ds.pixel_array)
    plt.show();

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-f', '--file', type=str, nargs='+',
                        required=True);

    args = parser.parse_args()
    for file in args.file:
       name, ext = os.path.splitext(file)
       if ext == '.nii':
          show_nii(file)
       elif ext == '.dcm':
          show_dcm(file)
       else:
          show_dcm(file)
        
