#!/usr/bin/env python
import argparse
import os
import pydicom
import nibabel as nib
import matplotlib.pyplot as plt
import sys
import numpy
numpy.set_printoptions(threshold=sys.maxsize)

def show_slices(slices):
   """ Function to display row of image slices """
   fig, axes = plt.plots(1)
   for i, slice in enumerate(slices):
       axes[i].imshow(slice.T, cmap="gray", origin="lower")

def show_nii(file):
    img = nib.load(file)
    # print(img)
    img_data = img.get_fdata()
    sp = img_data.shape
    print("{} shape is {}".format(file, sp))
    slice_2 = img_data[512, 512, 10]
    # print(slice_0)
    show_slices(slice_2)
    plt.draw();

def show_dcm(file):
    # from pydicom.data import get_testdata_files
    # filename = get_testdata_files("CT_small.dcm")[0]
    # ds = pydicom.dcmread(filename)
    # plt.imshow(ds.pixel_array, cmap=plt.cm.bone)
    # plt.show();
    ds = pydicom.dcmread(file)
    print(ds)
    print(ds.pixel_array.shape)
    plt.imshow(ds.pixel_array)
    plt.show();

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('-f', '--file', type=str,
                        required=True);
    args = parser.parse_args()
    name, ext = os.path.splitext(args.file)
    if ext == '.nii':
        show_nii(args.file)
    elif ext == '.dcm':
        show_dcm(args.file)