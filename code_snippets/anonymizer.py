#!/usr/bin/env python
import argparse
import os
import pydicom
import nibabel as nib
import matplotlib.pyplot as plt
import shutil
import sys
import numpy
numpy.set_printoptions(threshold=sys.maxsize)

# command to anonymizer a directory
# find . -name '*.dcm' -type f | xargs -d '\n' ~/code/segmentation/code_snippets/anonymizer.py -f

def process_dcm(file):
    tmp = '/tmp/dcmanonymizer.tmp'
    ds = pydicom.dcmread(file)

    # remove the following fields:
    # 'AdditionalPatientHistory' 'OtherPatientIDs',
    # 'PatientBirthDate', 'PatientID', 'PatientName',
    sensitive = ['AdditionalPatientHistory',
                 'OtherPatientIDs',
                 'PatientBirthDate',
                 'PatientID',
                 'PatientName']
    rewrite = False
    for sen in sensitive:
        if hasattr(ds, sen) and getattr(ds, sen) != '':
            print("has attr {} = {}".format(sen, getattr(ds, sen)))
            setattr(ds, sen, '')
            rewrite = True
    if rewrite:
        print("rewrite {}".format(file))
        ds.save_as(tmp)
        os.remove(file)
        shutil.move(tmp, file)

    if args.copy_prefix:
        iia = "ImagesInAcquisition"
        inn = "InstanceNumber"
        sd = "SeriesDescription" # LO: 'OAx HR T2 FSE'
        total = getattr(ds, iia)
        current = getattr(ds, inn)
        ser = getattr(ds, sd)
        if T2 in ser:
            save_png = True
            
            
        output = "{}-{}-{}.dcm".format(args.copy_prefix,current, total)
        base = os.path.dirname(file)
        p = os.path.join(base, output)
        print("save to {}".format(p))
        ds.save_as(p)
    # plt.imshow(ds.pixel_array)
    # plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='DICOM file Anonymizer')
    parser.add_argument('-f', '--file', type=str, nargs='+',
                        required=True);
    parser.add_argument('-c', '--copy-prefix', type=str, default="");
    args = parser.parse_args()
    for file in args.file:
       process_dcm(file)
