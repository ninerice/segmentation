#!/usr/bin/env python
import argparse
import os
import pydicom
import nibabel as nib
import matplotlib.pyplot as plt
import shutil
import sys
import numpy as np
np.set_printoptions(threshold=sys.maxsize)
import binascii
import shutil
import errno
from PIL import Image
import SimpleITK as sitk
import cv2
from skimage import filters
from skimage.measure import label,regionprops

'''
./gen_ground_truth_data.py -s ~/raw-data/2019.12/32_1/ -d ~/processed-data/test-norm

./gen_ground_truth_data.py -s ~/raw-data/300MRI/pre-* -d ~/processed-data/mhd-norm-256-0/ -m 24 --crop-cfg 0
./gen_ground_truth_data.py -s ~/raw-data/2019.12/* -d ~/processed-data/mhd-norm-256-0/ -m 24 --crop-cfg 0
./gen_ground_truth_data.py -s ~/raw-data/300MRI-413/* -d ~/processed-data/mhd-norm-256-0/ -m 24 --crop-cfg 0

'''

class MhdData:
    def __init__(self, mhd_size, crop_cfg):
        self.mhd_size = mhd_size
        self.pic_num = None
        self.added = 0
        self.dimension = None
        self.spacing = None
        self.origin = None
        self.array = None
        # XXX TODO: make row/col/top/left configurable
        if crop_cfg == 0:
            self.resize = 1
            self.row = 256
            self.col = 256
            self.top = 192
            self.left = 128
        elif crop_cfg == 1:
            self.resize = 2
            self.row = 256
            self.col = 256
            self.top = 0
            self.left = 0
        elif crop_cfg == 2:
            self.resize = 1
            self.row = 512
            self.col = 512
            self.top = 0
            self.left = 0
        else:
            assert False, str(cfg)
        self.fatal = False

    def add(self, file, row, col, spacing, origin,
            idx, array, pic_num):
        if self.pic_num:
            if self.pic_num != pic_num:
                print("wrong file number: {} vs {}, filename {}".format(self.pic_num, pic_num, file))
        else:
            self.pic_num = pic_num

        if self.dimension:
            assert self.dimension[0] <= row
            assert self.dimension[1] <= col
        else:
            if self.mhd_size:
                self.dimension = (self.row, self.col, self.mhd_size)
            else:
                self.dimension = (self.row, self.col, self.pic_num)

        if self.spacing:
            if self.spacing != spacing:
                print("spacing mismatch {} vs {}, {}".format(self.spacing, spacing, file))
        else:
            self.spacing = spacing

        if self.origin:
            # assert self.origin == origin, "{} {}".format(origin, self.origin)
            pass
        else:
            self.origin = origin

        # resize array
        if self.resize != 1:
            array = cv2.resize(array, dsize=(array.shape[0] // self.resize,
                                             array.shape[1] // self.resize),
                               interpolation=cv2.INTER_CUBIC)
        assert self.dimension[0] <= array.shape[0]
        assert self.dimension[1] <= array.shape[1]
        if self.array is None:
            self.array = np.zeros(self.dimension,
                                  dtype=array.dtype)
        if idx < self.mhd_size or self.mhd_size == 0:
            if array.shape < (self.top+self.row, self.left+self.col):
                print(self.array.shape, array.shape, self.top, self.left, self.row, self.col, self.resize, idx, self.mhd_size)
                self.fatal = True
            else:
                self.array[:, :, idx] = array[self.top:self.top+self.row,
                                              self.left:self.left+self.col]
        self.added += 1

    def save(self, filename, norm, contour, postfix):
        if self.fatal:
            print("fatal dimention error, cannot save {}".format(filename))
            return
        if self.added == 0:
            return
        if self.added < self.pic_num:
            print("not enough file: {} vs {}, filename {}".format(self.added, self.pic_num, filename))
        if self.mhd_size and self.mhd_size > self.added:
            for i in range(self.added, self.mhd_size):
                self.array[:, :, i] = self.array[:, :, self.added-1]
        array = np.transpose(self.array, (2, 0, 1))
        # set label to 1 instead of 255
        if np.count_nonzero(array) * 255 == np.sum(array) and np.max(array) == 255:
            array = (array // 255).astype(np.uint8)
        sitk_img = sitk.GetImageFromArray(array, isVector=False)
        sitk_img.SetSpacing(self.spacing)
        sitk_img.SetOrigin(self.origin)
        sitk.WriteImage(sitk_img, filename + postfix + ".mhd")
        print("saved {}".format(filename))
        if norm:
            # We shift the mean value to enhance the darker side
            UpperBound = 1.0
            LowerBound = -4.0
            Array = array
            """
            Array_new = Array.copy()
            Array_new -= np.min(Array_new)
            print("array new shape", Array_new.shape[0])
            Array_new = Array_new[Array_new.shape[0] // 2 - 5 :
                                  Array_new.shape[0] // 2 + 5]
            Mask = Array_new.copy()
            for i in range(Array_new.shape[0]):
                otsu = filters.threshold_otsu(Array_new[i])
                print(i, "otsu", otsu)
                print("average", np.average(Array_new[i]))
                print("min", np.min(Array_new[i]))
                print("max", np.max(Array_new[i]))
                Mask[i][Array_new[i]<0.5*otsu]=0
                Mask[i][Array_new[i]>=0.5*otsu]=1
            MaskSave = sitk.GetImageFromArray(Mask)
            MaskSave = sitk.BinaryDilate(MaskSave,10)
            MaskSave = sitk.BinaryErode(MaskSave,10)
            Mask = sitk.GetArrayFromImage(MaskSave)

            Avg = np.average(Array[Array_new.shape[0]//2-5:Array_new.shape[0]//2+5], weights=Mask)
            print("all avg weighted", Avg)
            print("all avg", np.average(Array[Array_new.shape[0]//2-5:Array_new.shape[0]//2+5]))
            Std = np.sqrt(np.average(abs(Array[Array_new.shape[0]//2-5:Array_new.shape[0]//2+5] - Avg)**2,weights=Mask))
            print("all std weighted", Std)
            print("std", np.sqrt(np.average(abs(Array[Array_new.shape[0]//2-5:Array_new.shape[0]//2+5] - Avg)**2)))
            Array = (Array.astype(np.float32)-Avg) / Std
            Array[Array>UpperBound]=UpperBound
            Array[Array<LowerBound]=LowerBound
            """
            Array=((Array.astype(np.float64)-np.min(Array))/(np.max(Array.astype(np.float64))-np.min(Array))*65535).astype(np.uint16)
            sitk_img = sitk.GetImageFromArray(Array, isVector=False)
            sitk_img.SetSpacing(self.spacing)
            sitk_img.SetOrigin(self.origin)
            sitk.WriteImage(sitk_img, norm + postfix + ".mhd")
        if contour:
            Label=array
            #Erode and Subtraction for contour label generation.
            #It works but may not be optimal.
            Contour=np.zeros(Label.shape,dtype=np.uint8)
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(3, 3))
            for z in range(Label.shape[0]):
                if np.sum(Label[z]>0):
                    LabelErode=Label[z]-cv2.erode(Label[z],kernel)
                    Contour[z]=LabelErode
            sitk_img = sitk.GetImageFromArray(Contour, isVector=False)
            sitk_img.SetSpacing(self.spacing)
            sitk_img.SetOrigin(self.origin)
            sitk.WriteImage(sitk_img, contour + postfix + ".mhd")

def is_gz_file(filepath):
    with open(filepath, 'rb') as test_f:
        return binascii.hexlify(test_f.read(2)) == b'1f8b'

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def find_ext_in_dir(ext, directory):
    result = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(ext):
                result.append(os.path.join(root, file))
    return result

def process_dcm(file, dest, prefix, mhd, expect_total, modular_in):
    ds = pydicom.dcmread(file)
    # from pydicom.pixel_data_handlers import gdcm_handler
    # ds.pixel_data_handlers = [gdcm_handler]

    iia = "ImagesInAcquisition"
    inn = "InstanceNumber"
    sd = "SeriesDescription" # LO: 'OAx HR T2 FSE'
    total = getattr(ds, iia)
    current = getattr(ds, inn)
    ser = getattr(ds, sd)
    if int(total) != expect_total and expect_total:
        return None, None
    if not modular_in:
        modular = ["OSag HR T2 FRFSE", "OAx HR T2 FSE", "OCor T2 FSE"]
    else:
        modular = modular_in
    # print("modular: {}".format(ser))
    ser = ser.strip().replace("  ", " ")
    ser = " ".join(ser.split(" "))
    match = False
    for i in modular:
        if i == "HR DWI":
            if ser.startswith(i):
                match = True
                break
        else:
            if i == ser:
                match = True
                break
    if match:
        # print("matched", i)
        if dest:
            for tp in ['png', 'tiff']:
                out_name = "{}__{}-{}.{}".format(prefix, current,
                                                total, tp)
                full_out_name = os.path.join(dest, out_name)
                if (tp == 'png'):
                    m = np.amax(ds.pixel_array)
                    # print(m)
                    d = (ds.pixel_array // 6).astype(np.uint8)
                    # d = np.stack((d,)*3, axis=-1)
                else:
                    d = ds.pixel_array.astype(np.uint16)
                    # d = np.stack((d,)*3, axis=-1)
                im = Image.fromarray(d)
                if not modular_in:
                    im.save(full_out_name)

            # print(file)
            mhd.add(file,
                    row=ds.Rows,
                    col=ds.Columns,
                    spacing=(float(ds.PixelSpacing[0]),
                             float(ds.PixelSpacing[1]),
                             float(ds.SliceThickness)),
                    origin=ds.ImagePositionPatient,
                    idx=int(current)-1,
                    array=ds.pixel_array,
                    pic_num=total)
        return True, int(total)
    else:
        if ser not in ["WATER:Ax LAVA Flex mask", "Ax DWI b=800",
                       "HR DWI", "HR DWI b=200", "HR DWI b=400", "HR DWI B=400", "HR DWI b=800", "HR DWI b=1200", "HR DWI b=1500", "HR DWI B=200",
                       "OAx HR T1 FSE", "OAx HR T1 FSE+C", 
                       "OSag HR T2 FRFSE", "OAx HR T2 FSE",
                       "OCor T2 FSE"]:
            print("other modular {}".format(ser))
            # assert False
        return None, None

def process_nii(file, dest, prefix, mhd_size, crop_cfg):
    img = nib.load(file)
    img_data = img.get_fdata()
    sp = img_data.shape
    mhd = MhdData(mhd_size, crop_cfg)
    # print("{} shape is {}, dtype={}".format(file, sp, img_data.dtype))
    first_big = -1
    first_big_small = -1
    first_big_small_big = -1
    if dest:
        for i in range(sp[2]):
            slice_2 = img_data[:, :, i] * 255
            slice_2 = np.transpose(slice_2)
            idx = sp[2] - i
            out_name = "{}__{}-{}.png".format(prefix, idx, sp[2])
            full_out_name = os.path.join(dest, out_name)
            sum = np.sum(slice_2) / 255
            if sum < 5:
                slice_2[:] = 0
            im = Image.fromarray(slice_2.astype(np.uint8))
            im.save(full_out_name)
            print("slice {}: {} points".format(idx, sum))
            if sum > 5:
                if first_big_small_big == -1:
                    if first_big_small >= 0:
                        first_big_small_big = idx
                        print("disconnected label {} between {} and {}".format(
                              prefix, first_big_small  + 1, first_big_small_big))
                    else:
                        if first_big == -1:
                            first_big = idx
            else:
                if first_big_small == -1:
                    if first_big != -1:
                        first_big_small = idx

            mhd.add(file,
                    row=slice_2.shape[0],
                    col=slice_2.shape[1],
                    spacing=(0.5,0.5,5),
                    origin=(0,0,0),
                    idx=idx-1,
                    array=slice_2,
                    pic_num=sp[2])
        mhd.save(os.path.join(dest, prefix),
                 None,
                 os.path.join(dest + "_contour", prefix), "")
    return sp[2]

def process_overlay(prefix, src0, src1, dest):
    labels = find_ext_in_dir("png", src1)
    image_count = 0
    for label_name in labels:
        base, ext = os.path.splitext(os.path.basename(label_name))
        if prefix not in base:
            continue
        # get image name
        image_name = os.path.join(src0, base) + ".tiff"
        # get overlay name
        overlay_name = os.path.join(dest, base) + ".png"
        img = np.asarray(Image.open(image_name))
        #img = img.convert("RGBA")
        background = np.asarray(Image.open(label_name))
        '''
        img2 = img.copy()
        img2[background[:] == 0] = 0
        m = np.amax(img2)
        img2[background[:] == 0] = 255
        n = np.amin(img2)
        print(np.amax(img), np.amax(img), m, n)
        '''
        img = img // 6
        background = background // 2
        #background = background.convert("RGBA")
        #new_img = Image.blend(background, img, 0.6)
        new_img = np.stack((background, img, background), axis=-1)
        #new_img.save(overlay_name, "PNG")
        Image.fromarray(new_img.astype(np.uint8)).save(overlay_name)
    return True

def process_cpng(prefix, src0):
    images = find_ext_in_dir("png", src0)

    print(prefix)
    images = [i for i in images if os.path.basename(i).startswith(prefix + '_')]

    for i in range(len(images)):
        n = [0, 0, 0]
        if i == 0:
            n[0] = 1
        else:
            n[0] = i
        n[1] = i + 1
        if i == len(images) - 1:
            n[2] = i + 1
        else:
            n[2] = i + 2
        imgs = []
        for k in range(3):
            in_name = os.path.join(src0, "{}__{}-{}.png".format(prefix, n[k], len(images)))
            imgs.append(np.asarray(Image.open(in_name)))
        new_img = np.stack((imgs[0], imgs[1], imgs[2]), axis=-1)
        out_name = os.path.join(src0, "{}__{}-{}.PNG".format(prefix, n[1], len(images)))
        Image.fromarray(new_img.astype(np.uint8)).save(out_name)
        print("save to {}".format(out_name))

    return True


def process(src, dest, prefix, mhd_size, crop_cfg, modular, modular_postfix):
    if dest:
        print("process {} save to {}".format(src, dest))
    if not os.path.isdir(src):
        "{} is not a directory".format(src)
        return False

    if dest:
        label_dest = os.path.join(dest, "label")
        mkdir_p(label_dest)
        mkdir_p(label_dest+"_contour")
        image_dest = os.path.join(dest, "image")
        mkdir_p(image_dest)
        mkdir_p(image_dest+"_norm")
        overlay_dest = os.path.join(dest, "overlay")
        mkdir_p(overlay_dest)
    else:
        label_dest = None
        image_dest = None
        overylay_dest = None

    # remove trailing '/'
    if src[-1] == '/':
        src = src[:-1]
    file_prefix = os.path.basename(src)
    if prefix:
        file_prefix = prefix + "-" + file_prefix
    # find nii file, if there is more than one, bail out
    niis = find_ext_in_dir("nii", src)
    if len(niis) > 2 or len(niis) == 0:
        print("ERROR: {} has bad nii files count {}".format(src, len(niis)))
        label_count = 0
        return False
    else:
        name = None
        if len(niis) == 2:
            for i in niis:
                if os.path.basename(i).lower().startswith('npre'):
                    if name is not None:
                        print("ERROR: {} has bad nii files count {} and double Npre: {}".format(src, len(niis), niis))
                        label_count = 0
                        return False
                    name = i
                else:
                    # print(i)
                    pass
            if name is None:
                print("ERROR: {} has bad nii files count {} and no Npre".format(src, len(niis)))
                label_count = 0
                return False
        else:
            name = niis[0]

        if not modular:
            label_count = process_nii(name, label_dest, file_prefix, mhd_size, crop_cfg)
        else:
            label_count = 0

    # find dcm files and process
    dcms = find_ext_in_dir("dcm", src)
    image_count = 0
    mhd = MhdData(mhd_size, crop_cfg)
    for dcm in dcms:
        res, total = process_dcm(dcm, image_dest, file_prefix,
                                 mhd, label_count, modular)
        if res and not modular:
            if total != label_count:
                print("{} has bad label/total-image count {} vs {}".format(src, label_count, total))
                # reset mhd
                mhd = MhdData(mhd_size, crop_cfg)
                continue
            image_count += 1

    # sanity check
    if not (label_count == image_count or modular):
        print("ERROR: {} has bad label/image count {} vs {}".format(src, label_count, image_count))
        return False
    else:
        if dest:
            mhd.save(os.path.join(image_dest, file_prefix),
                     os.path.join(image_dest + "_norm", file_prefix),
                     None,
                     modular_postfix)
            if not modular:
                process_cpng(file_prefix, image_dest)
                return process_overlay(file_prefix, image_dest,
                                       label_dest, overlay_dest)
            else:
                return True
        else:
            return True

if __name__ == "__main__":
    print('Number of arguments:', len(sys.argv), 'arguments.')
    print('Argument List:', str(sys.argv))
    parser = argparse.ArgumentParser(description='generate ground truth data')
    parser.add_argument('-s', '--source', type=str, nargs='+', required=True);
    parser.add_argument('-d', '--destination', type=str, default="");
    parser.add_argument('-p', '--prefix', type=str, default="");
    parser.add_argument('-m', '--mhd-size', type=int, default=0);
    parser.add_argument('-c', '--crop-cfg', type=int, default=0);
    parser.add_argument("-n", "--use-png", action="store_true")
    parser.add_argument("--modular", nargs='+', type=str, default="")
    parser.add_argument("--modular-postfix", type=str, default="")
    args = parser.parse_args()
    for source in args.source:
        if process(source, None, args.prefix, args.mhd_size, args.crop_cfg,
                   args.modular, args.modular_postfix):
            if args.destination:
                process(source, args.destination, args.prefix, args.mhd_size, args.crop_cfg,
                        args.modular, args.modular_postfix)
        else:
            print("bad source {}, skip".format(source))
