#!/usr/bin/env python
import argparse
import os
import sys
import shutil
import errno
import random

# cmd:
# ./gen_for_3D-RU-Net.py -s ~/processed-data/mhd-norm-256-0/ -d ~/processed-data/Data-256-0-1/

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def process_copy(src, dst, random_seed):
    print("use random seed {}".format(random_seed))
    random.seed(random_seed)
    src_dirs = ["image", "image_norm", "label", "label_contour"]
    dst_prefix = ["Image_1", "Image_2", "Label", "Contour"]
    all_files=os.listdir(os.path.join(src, src_dirs[0]))
    all_files_mhd = [ x for x in all_files if x.endswith(".mhd")]
    type_pos = ["Test", "Train", "Valid"]
    attn_type_pos = ["test", "train", "valid"]
    attn_ilos = ["image", "label", "overlay"]
    for f in all_files_mhd:
        basef = os.path.basename(f)
        base, _ = os.path.splitext(f)
        split = base.split("_")
        if split[-1] in ["T1", "T1C"]:
            continue
        r = random.randint(0, 100)
        if r < 15:
            type_idx = 0
        elif r < 85:
            type_idx = 1
        else:
            type_idx = 2
        for i in range(len(src_dirs)):
            s = os.path.join(src, src_dirs[i])
            d = os.path.join(dst, type_pos[type_idx], base, "HighRes");
            mkdir_p(d)
            for ext in [".raw", ".mhd"]:
                for MM in ["", "_T1", "_T1C"]:
                    base_2 = base + MM
                    sf = os.path.join(s, base_2 + ext)
                    df = os.path.join(d, dst_prefix[i] + MM + ext)
                    if not os.path.isfile(sf):
                        continue
                    print("copy {} to {}".format(sf, df))
                    if ext is ".raw":
                        shutil.copyfile(sf, df)
                    else:
                        with open(sf, "rt") as fin:
                            with open(df, "wt") as fout:
                                for line in fin:
                                    fout.write(line.replace(base_2 + ".raw", dst_prefix[i] + ".raw"))

        for ilo in attn_ilos:
            s = os.path.join(src, ilo)
            d = os.path.join(dst, 'attn', attn_type_pos[type_idx], ilo)
            mkdir_p(d)
            files=os.listdir(os.path.join(src, s))
            files_to_copy = [ x for x in files if x.startswith(base+'__')]
            for k in files_to_copy:
                basef = os.path.basename(k)
                shutil.copyfile(os.path.join(s, k),
                                os.path.join(d,k))

if __name__ == "__main__":
    print('Number of arguments:', len(sys.argv), 'arguments.')
    print('Argument List:', str(sys.argv))
    parser = argparse.ArgumentParser(description='generate data for 3D-RU-Net')
    parser.add_argument('-s', '--source', type=str, required=True);
    parser.add_argument('-d', '--destination', type=str, default="");
    parser.add_argument('-r', '--random-seed', type=int, default=1);
    args = parser.parse_args()
    process_copy(args.source, args.destination, args.random_seed)
