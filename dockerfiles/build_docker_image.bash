#!/bin/bash
#
# example: ./build_docker_image.sh Dockerfile__tf__tf15-gpu-jupyter-python36-vision
#

set -e

IFS='__' read -r -a ADDR <<< "$1"
docker build -f $1 -t temp-img .
REPO=${ADDR[2]}
TAG=${ADDR[4]}
TARGET=ninerice/$REPO:$TAG
echo build target: $TARGET
docker build -f $1 -t temp-img .
docker tag temp-img $TARGET
echo $TARGET is built. Use the following command to push
echo "  " docker push $TARGET
