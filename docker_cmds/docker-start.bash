#!/bin/bash
IMG=ninerice/tf:tf15-gpu-jupyter-python36-vision
docker pull $IMG && \
nvidia-docker run --runtime=nvidia  --rm -it -l tenx \
   -e DISPLAY=$DISPLAY \
   -v /tmp/.X11-unix:/tmp/.X11-unix \
   -u $(id -u ${USER}):$(id -g ${USER}) \
   -w "`pwd`" \
   -v /etc/group:/etc/group \
   -v /etc/passwd:/etc/passwd \
   -v /etc/passwd-:/etc/passwd- \
   -v /etc/shadow:/etc/shadow \
   -v /etc/group-:/etc/group- \
   -v /etc/sudoers:/etc/sudoers \
   -v /home:/home \
   -p 8888:8888 \
   $IMG \
   bash  --noprofile --norc
