#!/bin/bash
docker pull ninerice/tf:tf15-gpu-jupyter-python36-vision && \
nvidia-docker run --rm -it -l tenx \
   -e DISPLAY=$DISPLAY \
   -v /tmp/.X11-unix:/tmp/.X11-unix \
   -v /etc/group:/etc/group -w `pwd` \
   -v /etc/passwd:/etc/passwd \
   -v /home:/home \
   -p 6006:6006 \
   ninerice/tf:tf15-gpu-jupyter-python36-vision \
   bash
